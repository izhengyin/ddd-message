/**
 * 存放规约模式(Specification Pattern)的通用类
 * @author zhengyin
 * Created on 2021/7/2
 */
package com.izhengyin.dddmessage.domain.shared.specification;